from twython import Twython
import json
import pandas as pd
import twi_ds_cred as creds
    
python_tweets = Twython(creds.consumer_key, creds.consumer_secret)

query = {'q': 'learn python',
         'result_type': 'popular',
         'count': 10,
         'lang': 'en'}


dict_ = {'user': [], 'date': [], 'text': [], 'favorite_count': []}

for status in python_tweets.search(**query)['statuses']:
    dict_['user'].append(status['user']['screen_name'])
    dict_['date'].append(status['created_at'])
    dict_['text'].append(status['text'])
    dict_['favorite_count'].append(status['favorite_count'])
    
    
df = pd.DataFrame(dict_)
df.sort_values(by='favorite_count', inplace=True, ascending=False)
df.head(5)