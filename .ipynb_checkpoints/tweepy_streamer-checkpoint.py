from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream

import twi_ds_cred


class StdOutListener(StreamListener):

    def on_data(self, data):
        print(data)
        return True

    def on_error(self, status):
        print(status)


if __name__ == "__main__":

    listener = StdOutListener()
    auth = OAuthHandler(twi_ds_cred.consumer_key, twi_ds_cred.consumer_secret)
    auth.set_access_token(twi_ds_cred.access_token, twi_ds_cred.access_token_secret)

    stream = Stream(auth, listener)
    stream.filter(track=['#gambles', '#gamble', '#odds', '#bet', '#losses',
                         '#wager', '#lottery', '#risky', '#tradeoff', '#lucky',
                         '#random', '#casino', '#unlucky', '#betters', '#bets',
                         '#bingo', '#banked', '#banking', '#banker', '#dice',
                         '#gambler', '#poker', '#roulette', '#winnings', '#NFL',
                         '#packers', '#nfl100'])
