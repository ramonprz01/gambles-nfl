from twython import TwythonStreamer
import twi_ds_cred as creds
import json


def process_tweet(tweet):
    d = {}
    d['hashtags'] = [hashtag['text'] for hashtag in tweet['entities']['hashtags']]
    d['text'] = tweet['text']
    d['user'] = tweet['user']['screen_name']
    d['user_loc'] = tweet['user']['location']
    d['date'] = tweet['created_at']
    return d

class MyStreamer(TwythonStreamer):
    
    def on_success(self, data):
        
        if data['lang'] == 'en':
            tweet_data = process_tweet(data)
            self.save_to_csv(tweet_data)
            
    def on_error(self, status_code, data):
            print(status_code, data)
            self.disconnect()
            
            
    def save_to_csv(self, tweet):
        with open(r'saved_tweets.csv', 'a', encoding="utf-8") as file:
            writer = csv.writer(file)
            writer.writerow(list(tweet.values()))



            
            
stream = MyStreamer(creds.consumer_key, creds.consumer_secret,
                    creds.access_token, creds.access_token_secret)


            
stream.statuses.filter(track="chiefs gamble, chiefs, chiefs bet, chiefs bets, chiefs wager, chiefs nfl, chiefs nfl gamble, chiefs superbowl,\
                              texans gamble, texans bets, texans gambles, texans bets, texans nfl, texans, texans wager, texans odds, texans\
                              money, chiefs money, texans superbowl, texans punt, chiefs punt, chiefs win, texans win, texans long shot, chiefs\
                              long shot -filter:retweets")
