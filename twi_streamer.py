import os
import tweepy as tw
import pandas as pd
import twi_ds_cred as cred


auth = tw.OAuthHandler(cred.consumer_key, cred.consumer_secret)
auth.set_access_token(cred.access_token, cred.access_token_secret)
api = tw.API(auth, wait_on_rate_limit=True)


search_words = "chiefs gamble OR chiefs bet OR chiefs odds OR chiefs OR \
                texans OR texans gamble OR texans odds OR texans bet OR houstontexans -filter:retweets"

date_since = '2019-12-30'


tweets = tw.Cursor(api.search,
                   q=search_words,
                   lang="en",
                   since=date_since)


df = pd.DataFrame(data = [tweet.text for tweet in tweets], columns = ['tweets'])
        
df['id'] = np.array([tweet.id for tweet in tweets])
df['len'] = np.array([len(tweet.text) for tweet in tweets])
df['date'] = np.array([tweet.created_at for tweet in tweets])
df['source'] = np.array([tweet.source for tweet in tweets])
df['likes'] = np.array([tweet.favorite_count for tweet in tweets])
df['retweet'] = np.array([tweet.retweet_count for tweet in tweets])